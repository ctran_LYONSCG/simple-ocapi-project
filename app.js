// Import modules
var rp = require('request-promise');
var express = require('express');
var bodyparser = require('body-parser');
var cookieparser = require('cookie-parser');

// CONSTANTS
const OCAPI_HOST = 'http://lyons5.evaluation.dw.demandware.net/s/SiteGenesis//dw/shop/v17_4/';
const TEST_HOST = 'http://www.google.com';

// Instantiations
app = express();
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}));
app.use(cookieparser());
app.set('view engine', 'ejs');
app.use(express.static('public'));

function getProductImage(product){
// This function will map each product object and get relevant information
    // request for product image
    rp(OCAPI_HOST + 'products/' + product.product_id +'/images?client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' )
    .then((productResult) => {
        productResult = JSON.parse(productResult);
        product.product_image_link_m = productResult.image_groups[1].images[0].link;
        console.log(productsList[hit].product_image_link_m);                
    })
    .catch((err) => {
        console.log('Err getting product image link. Product ID: ' + product.product_id);
    });
    // request for product price
    rp(OCAPI_HOST + 'products/' + product.product_id +'/prices?client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' )
    .then((productResult) => {
        productResult = JSON.parse(productResult);
        product.price = productResult.price;
    })
    .catch((err) => {
        console.log('Error');
    });
}

var categoryHits = [],
    searchHits = [];


//-----search result page-----//
app.get('/search', (req, res) => {
    console.log(JSON.stringify(req.query.search));
    rp(OCAPI_HOST + '/product_search?' + 'q=' + req.query.search + '&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
    .then((result) => {
        var hits = JSON.parse(result).hits; // hits is now an array of products returned from search result
        // parse and get the hits returned for categorical search
        let productsList = [];
        // loop through hits to extract useful information about each product
        for (var hit in hits){
            productsList.push(hits[hit]);
        }

        productsList.map((product) => {getProductImage(product)});
        setTimeout(() => {res.render('search-result.ejs', {productsList: productsList})},2000);
    })
    .catch((err) => {
        console.log('Exception caught: ' + err + '\n' +'Path' + req.params.category);
    });
});

//-----categories routes----//
app.get('/:category', (req, res) => {
    rp(OCAPI_HOST + '/product_search?' + 'refine_1=cgid=' + req.params.category + '&levels=3&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
    .then((result) => {
        // parse and get the hits returned for categorical search
        let parsed = JSON.parse(result);
        let hits = parsed.hits;
        let productsList = [];
        // loop through hits to extract useful information about each product
        for (var hit in hits){
            productsList.push(hits[hit]);
        }

        productsList.map((product) => {getProductImage(product)});
        console.log(productsList);
        setTimeout(() => {res.render('category.ejs', {productsList: productsList})},2000);
    })
    .catch((err) => {
        console.log('Exception caught: ' + err + '\n' +'Path' + req.params.category);
    });
});

//------homepage----//
app.get('/', (req, res) => {
    rp(OCAPI_HOST + 'categories/root?levels=3&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
    .then((result) => {
        let parsed = JSON.parse(result);
        let categoriesRoot = parsed.categories;
        // res.send(categoriesRoot);
        res.render('index.ejs', {categoriesRoot: categoriesRoot});
    })
    .catch((err) => {
        res.send('<pre>' + JSON.stringify(err, null, 2) + '</pre>');
    });
});

// Server start to listen
app.listen(8080, () => console.log('Express server on port 8080...'))


